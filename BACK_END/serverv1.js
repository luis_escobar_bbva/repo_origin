var express = require ('express');
require('dotenv').config();
var userFile = require('./user.json');
var bodyParser = require('body-parser');
var URL_DB = "http://api.mlab.com/api/1/databases/techu7db/collections";
var API_KEY_DB = "apiKey="+process.env.MLAB_API_KEY;
var app = express();
app.use(bodyParser.json());
//app.use(express.urlencoded());
var totalUsers=0;
const URLbase = "/apitechu/v1/";
const DEFAULT_PORT = 3000;
const PORT = process.env.PORT || DEFAULT_PORT;

app.get ("/",
        function(req,res){
           res.send('Hello Lima');
});

//Peticion GET de todos los 'user'(Colllection)
app.get (URLbase+"users",
        function(req,res){
           console.log("GET "+ URLbase + "users");
           res.send(userFile);
});

/*
*QueryString for users
*Parametros posibles: first_name,email,init,pageSize
*/

app.get (URLbase+"usersq",
        function(req,res){
           console.log("GET "+ URLbase + "usersq con query string");
           console.log(req.query);
           userFile = require('./user.json');
           var arrayResponse = userFile;
           if(req.query!=undefined){
             var first_name = req.query.first_name;
             if(first_name!=undefined){
               var arrayTemp=[];
               for(us of arrayResponse){
                 if(us.first_name==first_name){
                   arrayTemp.push(us);
                 }
               }
               arrayResponse=arrayTemp;
             }
             var email = req.query.email;
             if(email!=undefined){
               var arrayTemp=[];
               for(us of arrayResponse){
                 if(us.email==email){
                   arrayTemp.push(us);
                 }
               }
               arrayResponse=arrayTemp;
             }
           }
           var size=arrayResponse.length;
           var init=Number(req.query.init);
           var pageSize=Number(req.query.pageSize);
           if(init!=undefined && pageSize!=undefined){
             var arrayTemp=[];
             if(init<=arrayResponse.length){
               for(var i=0;i<pageSize;i++){
                   console.log(init+i);
                   if(init+i<=arrayResponse.length){
                     arrayTemp.push(arrayResponse[init+i-1]);
                   }
               }
             }
             arrayResponse=arrayTemp;
           }
           res.send({"data":arrayResponse,"size":size});
});


app.get (URLbase+"loggedusers",
        function(req,res){
           userFile = require('./user.json');
           console.log("GET "+ URLbase + "loggedusers");
           console.log(req.query);
           var arrayResponse = [];
           for(us of userFile){
             if(us.logged!=undefined){
                if(us.logged==true){
                  arrayResponse.push(us);
                }
             }
           }

           res.send(arrayResponse);
});

//Peticion GET de un 'user'
app.get (URLbase+"users/:id",
        function(req,res){
           userFile = require('./user.json');
           console.log("GET "+ URLbase + "users/id");
           let id = req.params.id;
           var user=undefined;
           for(us of userFile){
             if(us.user_id==id){
                user =us;
                break;
             }
           }
           let respuesta=(user!=undefined)?user:{"mensaje":"Recurso no encontrado"};
           res.status(222);
           res.send(respuesta);
           console.log(respuesta);
});

function maximoID(){
  var maximo = 0;
  userFile = require('./user.json');
  for(us of userFile){
    if(us.user_id>maximo){
      maximo = us.user_id;
    }
  }
  maximo = maximo +1;
  console.log("Maximo:"+maximo);
  return maximo;
}
//Peticion por post
app.post(URLbase+"users",
         function(req,res){
           console.log("POST "+ URLbase + "users");
           console.log(req.body);
           userFile = require('./user.json');
           if(validarUser(req.body)){
             let newUser = {
                  user_id : maximoID(),
                  first_name : req.body.first_name,
                  last_name : req.body.last_name,
                  email : req.body.email,
                  password : req.body.password
             };
             userFile.push(newUser);
             console.log(userFile);
             writeUserDataToFile(userFile);
             res.status(200);
             res.send({"newUSer":newUser,"mensaje":"usuario creado de manera exitosa"});
           }else {

             res.status(400);
             res.send({"mensaje":"No ha enviado todos los parametros"});
           }

         }
);

app.put(URLbase+"users",
         function(req,res){
           console.log("PUT "+ URLbase + "users");
           console.log(req.body);
           userFile = require('./user.json');
           var success = false;
           var updateUSer=undefined;
           for(us of userFile){
             if(us.user_id==req.body.user_id){
               if(validarUser(req.body)){
                 us.user_id = req.body.user_id;
                 us.first_name = req.body.first_name;
                 us.last_name = req.body.last_name;
                 us.email = req.body.email;
                 us.password = req.body.password;
                 success=true;
                 updateUSer = us;
                 writeUserDataToFile(userFile);
               }else {
                 res.status(400);
                 res.send({"mensaje":"No ha enviado todos los parametros"});
               }

             }
           }
           if(success){
              res.status(201);
              res.send({"updateUSer":updateUSer,"mensaje":"usuario actualizado de manera exitosa"});
           }else {
             res.status(400);
             res.send({"mensaje":"No existe usuario con el ID indicado"});
           }

         }
);

app.put(URLbase+"users/:id",
         function(req,res){
           console.log("PUT "+ URLbase + "users/id");
           console.log(req.body);
           userFile = require('./user.json');
           totalUsers = userFile.length;
           let userID=req.params.id;
           if(userID!=undefined){
             var success = false;
             var updateUSer=undefined;
             for(us of userFile){
               if(us.user_id==userID){
                 if(validarUser(req.body)){
                   us.user_id = userID;
                   us.first_name = req.body.first_name;
                   us.last_name = req.body.last_name;
                   us.email = req.body.email;
                   us.password = req.body.password;
                   success=true;
                   updateUSer = us;
                   writeUserDataToFile(userFile);
                 }else {
                   res.status(400);
                   res.send({"mensaje":"No ha enviado todos los parametros"});
                 }

               }
             }
             if(success){
                res.status(201);
                res.send({"updateUSer":updateUSer,"mensaje":"usuario actualizado de manera exitosa"});
             }else {
               res.status(400);
               res.send({"mensaje":"No existe usuario con el ID indicado"});
             }
           }else {
             res.status(400);
             res.send({"mensaje":"Debe enviar el campo ID"});
           }


         }
);

app.delete(URLbase+"users/:id",
         function(req,res){
           console.log("Delete "+ URLbase + "users/id");
           userFile = require('./user.json');
           let userID=req.params.id;
           var indice=-1;
           if(userID!=undefined){
             for(var i=0;i<userFile.length;i++){
               if(userFile[i].user_id==userID){
                 indice=i;
               }
             }
             if(indice!=-1){
                userFile.splice(indice,1);
                console.log(userFile);
                writeUserDataToFile(userFile);
                res.status(200);
                res.send({"mensaje":"usuario "+userID+" eliminado de manera exitosa"});
             }else {
                res.status(400);
                res.send({"mensaje":"No existe usuario con el ID indicado"});
             }

           }else{
             res.status(400);
             res.send({"mensaje" : "No se ha especificado USERID"});
           }

         }
);


function validarUser(body){
  let flag = true;
  if(body==undefined){flag=false;}
  if(body.first_name==undefined||body.first_name.trim()==""){flag=false;}
  if(body.last_name==undefined||body.last_name.trim()==""){flag=false;}
  if(body.email==undefined||body.email.trim()==""){flag=false;}
  if(body.password==undefined||body.password.trim()==""){flag=false;}
  return flag;
}

//Peticion por post
app.post(URLbase+"login",
         function(req,res){
           console.log("POST "+ URLbase + "login");
           console.log(req.body);
           var cntBody = Object.keys(req.body).length;
           if (cntBody > 0){
             if(req.body.email!=undefined && req.body.password!=undefined){
               let email= req.body.email;
               let password = req.body.password;
               var success = false;
               var userID;
               for(us of userFile){
                 if(us.email==email && us.password==password){
                   success = true;
                   us.logged=true;
                   userID=us.user_id;
                   writeUserDataToFile(userFile);
                   break;
                 }
               }
               if(success){
                 res.status(200);
                 res.send({"mensaje" : "Login correcto","userID":userID});
               }else{
                 res.status(200);
                 res.send({"mensaje" : "Email o Password Incorrecta"});
               }
             }else{
               res.status(400);
               res.send({"mensaje" : "Debe Enviar campos de email y password"});
             }
           }else{
             res.status(400);
             res.send({"mensaje" : "No Existe parametros para Body"});
           }
         }
);

app.post(URLbase+"logout/:id",
         function(req,res){
           console.log("POST "+ URLbase + "logout/id");
           var userID = req.params.id;
           if(userID!=undefined){
             var success = false;
             for(us of userFile){
               if(us.user_id==userID && us.logged==true){
                 success = true;
                 delete us.logged;
                 writeUserDataToFile(userFile);
                 break;
               }
             }
             if(success){
               res.status(200);
               res.send({"mensaje" : "Se realizo el logout de manera satisfactoria"});
             }else{
               res.status(200);
               res.send({"mensaje" : "El usuario indicado no esta logueado"});
             }
           }else{
             res.status(400);
             res.send({"mensaje" : "Debe Enviar campo userID"});
           }
         }
);


function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 }

app.listen(PORT,function(){
    console.log("API TECHU escuchando en puerto 3000");
});
