var requestJSON = require('request-json');
require('dotenv').config();
var URL_DB_MLAB = "https://api.mlab.com/api/1/databases/techu7db/collections/";
var API_KEY_DB = "apiKey="+process.env.MLAB_API_KEY;

//Peticion GET de todos los 'user'(Colllection
function getUsers(req,res){
   console.log("GET /apicol/v3/users");
   var httpClient = requestJSON.createClient(URL_DB_MLAB);
   console.log("Cliente HTTP mLab creado.");
   var queryString = 'f={"_id":0}&';
   httpClient.get('user?' + queryString + API_KEY_DB,
     function(err, respuestaMLab, body) {
       console.log('Error: ' + err);
       console.log('Respuesta MLab: ' + respuestaMLab);
       console.log('Body: ' + body);
       // respuesta = !err ? body : {"msg" : "Error al recuperar users de mLab."};
       var response = {};
       if(err) {
           response = {
             "msg" : "Error obteniendo usuario."
           }
           res.status(500);
       } else {
         if(body.length > 0) {
           response = body;
         } else {
           response = {
             "msg" : "Usuario no encontrado."
           };
           res.status(404);
         }
       }
       res.send(response);
     });
   }


   function getLoggedUsers(req,res){
      console.log("GET /apicol/v3/loggedusers");
      var httpClient = requestJSON.createClient(URL_DB_MLAB);
      console.log("Cliente HTTP mLab creado.");
      var queryQString = 'q={"logged":true}&';
      var queryString = 'f={"_id":0}&';
      httpClient.get('user?'+queryQString+ queryString + API_KEY_DB,
        function(err, respuestaMLab, body) {
          console.log('Error: ' + err);
          console.log('Respuesta MLab: ' + respuestaMLab);
          console.log('Body: ' + body);
          // respuesta = !err ? body : {"msg" : "Error al recuperar users de mLab."};
          var response = {};
          if(err) {
              response = {
                "msg" : "Error obteniendo usuarios."
              }
              res.status(500);
          } else {
            if(body.length > 0) {
              response = body;
            } else {
              response = {
                "msg" : "Usuario no encontrado."
              };
              res.status(404);
            }
          }
          res.send(response);
        });
      }

     function logoutUser(req,res){
       let userID=req.params.id;
       var indice=-1;
       if(userID!=undefined){
          var id = req.params.id;
          var queryStringID = 'q={"user_id":' + id + ',"logged":true}&';
          console.log(URL_DB_MLAB + 'user?' + queryStringID + API_KEY_DB);
          var httpClient = requestJSON.createClient(URL_DB_MLAB);
          httpClient.get('user?' +  queryStringID + API_KEY_DB,
            function(error, respuestaMLab, body){
              if(body[0]!=undefined){
                let logout = '{"$unset":{"logged":1}}';
                httpClient.put('user?q={"user_id": ' + body[0].user_id + '}&' + API_KEY_DB, JSON.parse(logout),
                //clienteMlab.put('user/' + body[0]._id.$oid + '?' + API_KEY_DB, JSON.parse(login),
                  function(errPut, resPut, bodyPut) {
                    res.send({'msg':'Logout correcto', 'user':body[0].email, 'userid':body[0].user_id});
                    // If bodyPut.n == 1, put de mLab correcto
                  });
              }else{
                res.status(400);
                res.send({"mensaje" : "No se ha encontrado un usuario logueado"});
              }

            });

       }else{
         res.status(400);
         res.send({"mensaje" : "No se ha especificado USERID"});
       }
     }

     module.exports.getUsers = getUsers;
     module.exports.getLoggedUsers = getLoggedUsers;
     module.exports.logoutUser = logoutUser;
