const user_controller = require('./controllers/user_controller');
var express = require ('express');
var userFile = require('./user.json');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
var URL_DB_MLAB = "https://api.mlab.com/api/1/databases/techu7db/collections/";
var API_KEY_DB = "apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF";
var app = express();
app.use(bodyParser.json());
//app.use(express.urlencoded());
var totalUsers=0;
const URLbase = "/apitechu/v1/";
const DEFAULT_PORT = 3000;
const PORT = process.env.PORT || DEFAULT_PORT;

app.get ("/",
        function(req,res){
           res.send('Hello Lima');
});

//Peticion GET de todos los 'user'(Colllection)
app.get (URLbase+"users",
        function(req,res){
          user_controller.getUsers(req,res);
});

/*
*QueryString for users
*Parametros posibles: first_name,email,init,pageSize
*/

app.get (URLbase+"usersq",
        function(req,res){
           console.log("GET "+ URLbase + "usersq con query string");
           console.log(req.query);
           userFile = require('./user.json');
           var arrayResponse = userFile;
           if(req.query!=undefined){
             var first_name = req.query.first_name;
             if(first_name!=undefined){
               var arrayTemp=[];
               for(us of arrayResponse){
                 if(us.first_name==first_name){
                   arrayTemp.push(us);
                 }
               }
               arrayResponse=arrayTemp;
             }
             var email = req.query.email;
             if(email!=undefined){
               var arrayTemp=[];
               for(us of arrayResponse){
                 if(us.email==email){
                   arrayTemp.push(us);
                 }
               }
               arrayResponse=arrayTemp;
             }
           }
           var size=arrayResponse.length;
           var init=Number(req.query.init);
           var pageSize=Number(req.query.pageSize);
           if(init!=undefined && pageSize!=undefined){
             var arrayTemp=[];
             if(init<=arrayResponse.length){
               for(var i=0;i<pageSize;i++){
                   console.log(init+i);
                   if(init+i<=arrayResponse.length){
                     arrayTemp.push(arrayResponse[init+i-1]);
                   }
               }
             }
             arrayResponse=arrayTemp;
           }
           res.send({"data":arrayResponse,"size":size});
});


app.get (URLbase+"loggedusers",
        function(req,res){
           user_controller.getLoggedUsers(req,res);
});

//Peticion GET de un 'user'
app.get (URLbase+"users/:id",
        function(req,res){
            var httpClient = requestJSON.createClient(URL_DB_MLAB);
            console.log("Cliente HTTP mLab creado.");
            var queryQString = 'q={"user_id":'+req.params.id+'}&';
            var queryFString = 'f={"_id":0}&';
            console.log('user?' +queryQString+ queryFString + API_KEY_DB);
            httpClient.get('user?' +queryQString+ queryFString + API_KEY_DB,
            function(err, respuestaMLab, body) {
              console.log('Error: ' + err);
              console.log('Respuesta MLab: ' + respuestaMLab);
              console.log('Body: ' + body);
              var response = {};
              if(err) {
                  response = {
                    "msg" : "Error obteniendo usuario."
                  }
                  res.status(500);
              } else {
                if(body.length > 0) {
                  response = body;
                } else {
                  response = {
                    "msg" : "Usuario no encontrado."
                  };
                  res.status(404);
                }
              }
              res.send(response);
            });
});

app.get (URLbase+"users/:id/accounts",
        function(req,res){
            var httpClient = requestJSON.createClient(URL_DB_MLAB);
            var queryQString = 'q={"user_id":'+req.params.id+'}&';
            var queryFString = 'f={"_id":0}&';
            httpClient.get('account?' +queryQString+ queryFString + API_KEY_DB,
            function(err, respuestaMLab, body) {
              console.log('Error: ' + err);
              console.log('Respuesta MLab: ' + respuestaMLab);
              console.log('Body: ' + body);
              var response = {};
              if(err) {
                  response = {
                    "msg" : "Error obteniendo cuentas."
                  }
                  res.status(500);
              } else {
                if(body.length > 0) {
                  response = body;
                } else {
                  response = {
                    "msg" : "Cuentas no encontradas."
                  };
                  res.status(404);
                }
              }
              res.send(response);
            });
});

function maximoID(){
  var maximo = 0;
  userFile = require('./user.json');
  for(us of userFile){
    if(us.user_id>maximo){
      maximo = us.user_id;
    }
  }
  maximo = maximo +1;
  console.log("Maximo:"+maximo);
  return maximo;
}
//Peticion por post
app.post(URLbase+"users",
         function(req,res){
           console.log("POST "+ URLbase + "users");
           console.log(req.body);
           userFile = require('./user.json');
           if(validarUser(req.body)){

             var clienteMlab = requestJSON.createClient(URL_DB_MLAB);
             console.log(req.body);
             clienteMlab.get('user?' + API_KEY_DB,
               function(error, respuestaMLab, body){
                 console.log(URLbase+'user?' + API_KEY_DB);
                 newID = body.length + 1;
                 console.log("newID:" + newID);
                 var newUser = {
                   "user_id" : newID,
                   "first_name" : req.body.first_name,
                   "last_name" : req.body.last_name,
                   "email" : req.body.email,
                   "password" : req.body.password
                 };
                 clienteMlab.post("user?" + API_KEY_DB, newUser,
                   function(error, respuestaMLab, body){
                     console.log(body);
                     res.status(201);
                     res.send(body);
                   });
               });

           }else {

             res.status(400);
             res.send({"mensaje":"No ha enviado todos los parametros"});
           }

         }
);

app.put(URLbase+"users",
         function(req,res){
           console.log("PUT "+ URLbase + "users");
          var id = req.body.id;
          var queryStringID = 'q={"user_id":' + id + '}&';
          var clienteMlab = requestJSON.createClient(URL_DB_MLAB);
          clienteMlab.get('user?'+ queryStringID + API_KEY_DB,
           function(error, respuestaMLab, body) {
            var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
            console.log(req.body);
            console.log(cambio);
            clienteMlab.put(URL_DB_MLAB +'user?' + queryStringID + API_KEY_DB, JSON.parse(cambio),
             function(error, respuestaMLab, body) {
               console.log("body:"+ body); // body.n devuelve 1 si pudo hacer el update
              //res.status(200).send(body);
              res.send(body);
             });
           });

         }
);

app.put(URLbase+"users/:id",
         function(req,res){
           console.log("PUT "+ URLbase + "users/id");
           console.log(req.body);
           userFile = require('./user.json');
           totalUsers = userFile.length;
           let userID=req.params.id;
           if(userID!=undefined){
             var id = userID;
             var queryStringID = 'q={"user_id":' + id + '}&';
             var clienteMlab = requestJSON.createClient(URL_DB_MLAB);
             clienteMlab.get('user?'+ queryStringID + API_KEY_DB,
              function(error, respuestaMLab, body) {
               var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
               console.log(URL_DB_MLAB+'user?'+ queryStringID + API_KEY_DB);
               console.log(req.body);
               console.log(cambio);
               clienteMlab.put(URL_DB_MLAB +'user?' + queryStringID + API_KEY_DB, JSON.parse(cambio),
                function(error, respuestaMLab, body) {
                  console.log("body:"+ body); // body.n devuelve 1 si pudo hacer el update
                 //res.status(200).send(body);
                 res.send(body);
                });
              });
           }else {
             res.status(400);
             res.send({"mensaje":"Debe enviar el campo ID"});
           }


         }
);

app.put(URLbase + 'usersmLab/:id',
   function (req, res) {
     var id = req.params.id;
     let userBody = req.body;
     var queryString = 'q={"user_id":' + id + '}&';
     var httpClient = requestJSON.createClient(URL_DB_MLAB);
     httpClient.get('user?' + queryString + API_KEY_DB,
       function(err, respuestaMLab, body){
         let response = body[0];
         console.log(body);
         //Actualizo campos del usuario
         let updatedUser = {
           "user_id" : req.body.id,
           "first_name" : req.body.first_name,
           "last_name" : req.body.last_name,
           "email" : req.body.email,
           "password" : req.body.password
         };//Otra forma simplificada (para muchas propiedades)
         // var updatedUser = {};
         // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
         // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
         // PUT a mLab
         httpClient.put('user/' + response._id.$oid + '?' + API_KEY_DB, updatedUser,
           function(err, respuestaMLab, body){
             var response = {};
             if(err) {
                 response = {
                   "msg" : "Error actualizando usuario."
                 }
                 res.status(500);
             } else {
               if(body.length > 0) {
                 response = body;
               } else {
                 response = {
                   "msg" : "Usuario actualizado correctamente."
                 }
                 res.status(200);
               }
             }
             res.send(response);
           });
       });
 });


app.delete(URLbase+"users/:id",
         function(req,res){
           console.log("Delete "+ URLbase + "users/id");
           let userID=req.params.id;
           var indice=-1;
           if(userID!=undefined){
              var id = req.params.id;
              var queryStringID = 'q={"user_id":' + id + '}&';
              console.log(URL_DB_MLAB + 'user?' + queryStringID + API_KEY_DB);
              var httpClient = requestJSON.createClient(URL_DB_MLAB);
              httpClient.get('user?' +  queryStringID + API_KEY_DB,
                function(error, respuestaMLab, body){
                  var respuesta = body[0];
                  httpClient.delete("user/" + respuesta._id.$oid +'?'+ API_KEY_DB,
                    function(error, respuestaMLab,body){
                      res.send(body);
                  });
                });

           }else{
             res.status(400);
             res.send({"mensaje" : "No se ha especificado USERID"});
           }

         }
);


function validarUser(body){
  let flag = true;
  if(body==undefined){flag=false;}
  if(body.first_name==undefined||body.first_name.trim()==""){flag=false;}
  if(body.last_name==undefined||body.last_name.trim()==""){flag=false;}
  if(body.email==undefined||body.email.trim()==""){flag=false;}
  if(body.password==undefined||body.password.trim()==""){flag=false;}
  return flag;
}

//Peticion por post
app.post(URLbase+"login",
         function(req,res){
           console.log("POST "+ URLbase + "login");
           console.log(req.body);
           var cntBody = Object.keys(req.body).length;
           if (cntBody > 0){
             if(req.body.email!=undefined && req.body.password!=undefined){
               let email= req.body.email;
               let password = req.body.password;
              let queryString = 'q={"email":"' + email + '","password":"' + password + '"}&';
              let limFilter = 'l=1&';
              let clienteMlab = requestJSON.createClient(URL_DB_MLAB);
              clienteMlab.get('user?'+ queryString + limFilter + API_KEY_DB,
                function(error, respuestaMLab, body) {
                  if(!error) {
                    if (body.length == 1) { // Existe un usuario que cumple 'queryString'
                      let login = '{"$set":{"logged":true}}';
                      clienteMlab.put('user?q={"user_id": ' + body[0].user_id + '}&' + API_KEY_DB, JSON.parse(login),
                      //clienteMlab.put('user/' + body[0]._id.$oid + '?' + API_KEY_DB, JSON.parse(login),
                        function(errPut, resPut, bodyPut) {
                          res.send({'msg':'Login correcto', 'user':body[0].email, 'userid':body[0].id});
                          // If bodyPut.n == 1, put de mLab correcto
                        });
                    }
                    else {
                      res.status(404).send({"msg":"Usuario no válido."});
                    }
                  } else {
                    res.status(500).send({"msg": "Error en petición a mLab."});
                  }
              });
             }else{
               res.status(400);
               res.send({"mensaje" : "Debe Enviar campos de email y password"});
             }
           }else{
             res.status(400);
             res.send({"mensaje" : "No Existe parametros para Body"});
           }
         }
);

app.post(URLbase+"logout/:id",
         function(req,res){
           user_controller.logoutUser(req,res);
         }
);


function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 }

app.listen(PORT,function(){
    console.log("API TECHU escuchando en puerto 3000");
});
