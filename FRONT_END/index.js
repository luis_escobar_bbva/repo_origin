function guardarEnLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */

  var clave = txtClave.value;
  var valor = txtValor.value;

  localStorage.setItem(clave, valor);

  var valor = {
    "nombre":"Ezequiel",
    "apellidos":"Llarena Borges",
    "ciudad":"Madrid",
    "pais":"España"
  };

  localStorage.setItem("json", JSON.stringify(valor));


}

function leerDeLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = localStorage.getItem(clave);

  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = valor;

  var datosUsuario = JSON.parse(localStorage.getItem("json"));
  console.log(datosUsuario.nombre);
  console.log(datosUsuario.pais);
  console.log(datosUsuario);
}

function guardarEnSessionStorage() {
  var txtClave = document.getElementById("txtClaveSession"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValorSession"); /* Referencia al input de valor */

  var clave = txtClave.value;
  var valor = txtValor.value;

  sessionStorage.setItem(clave, valor);

  var valor = {
    "nombre":"Ezequiel",
    "apellidos":"Llarena Borges",
    "ciudad":"Madrid",
    "pais":"España"
  };

  sessionStorage.setItem("json", JSON.stringify(valor));
}

function leerDeSessionStorage() {
  var txtClave = document.getElementById("txtClaveSession"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = sessionStorage.getItem(clave);

  var spanValor = document.getElementById("spanValorSession");
  spanValor.innerText = valor;

  var datosUsuario = JSON.parse(sessionStorage.getItem("json"));
  console.log(datosUsuario.nombre);
  console.log(datosUsuario.pais);
  console.log(datosUsuario);
}
