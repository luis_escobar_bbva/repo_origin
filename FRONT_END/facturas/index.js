var idsFacturas =[];

window.onload = function() {
  llenarTabla();
  idsFacturas =[];
};


function guardarFactura() {
  var txtCodigo = document.getElementById("txtCodigo"); /* Referencia al input de clave */
  var txtFecha = document.getElementById("txtFecha"); /* Referencia al input de valor */
  var txtTotal = document.getElementById("txtTotal");

  var codigo = txtCodigo.value;
  var fecha = txtFecha.value;
  var total = txtTotal.value;


  var factura = {
    codigo:codigo,
    fecha:fecha,
    total:total
  };
  idsFacturas.push(codigo);
  localStorage.setItem(codigo, JSON.stringify(factura));
  localStorage.setItem("idsFacturas", JSON.stringify(idsFacturas));
  alert("factura guardada")
  setCampos("","","");
  llenarTabla();
}

function visualizarFactura() {
  var txtCodigo = document.getElementById("txtCodigo"); /* Referencia al input de clave */


  var factura = JSON.parse(localStorage.getItem(txtCodigo.value));
  setCampos(factura.codigo,factura.fecha,factura.total);
}

function setCampos(codigo,fecha,total){
  var txtCodigo = document.getElementById("txtCodigo"); /* Referencia al input de clave */
  var txtFecha = document.getElementById("txtFecha"); /* Referencia al input de valor */
  var txtTotal = document.getElementById("txtTotal");
  txtCodigo.value=codigo;
  txtFecha.value=fecha;
  txtTotal.value =total;
}

function eliminarFactura(){
  var txtCodigo = document.getElementById("txtCodigo"); /* Referencia al input de clave */
  if(localStorage.getItem("idsFacturas")!=""){
    idsFacturas=JSON.parse(localStorage.getItem("idsFacturas"));
  }else{
    idsFacturas=[];
  }
  for(var i=0;i<idsFacturas.length;i++){
    if(txtCodigo.value==idsFacturas[i]){
      idsFacturas.splice(i,1);
    }
  }
  localStorage.setItem("idsFacturas", JSON.stringify(idsFacturas));

  localStorage.removeItem(txtCodigo.value);
  llenarTabla();
}

function llenarTabla(){
  if(localStorage.getItem("idsFacturas")!=""){
    idsFacturas=JSON.parse(localStorage.getItem("idsFacturas"));
  }else{
    idsFacturas=[];
  }
  while(document.getElementById('tablaFacturas').rows.length>0){
    document.getElementById('tablaFacturas').deleteRow(0);
  }

  var r=document.getElementById('tablaFacturas').insertRow(0);
  var x = r.insertCell(0);
  var y = r.insertCell(1);
  var z = r.insertCell(2);
  x.innerHTML="Codigo";
  y.innerHTML="Fecha";
  z.innerHTML="Total";


  if(idsFacturas!=null){
    for(var i=0;i<idsFacturas.length;i++){
      var factura = JSON.parse(localStorage.getItem(idsFacturas[i]));
      r=document.getElementById('tablaFacturas').insertRow(i+1);
      x = r.insertCell(0);
      y = r.insertCell(1);
      z = r.insertCell(2);
      x.innerHTML=factura.codigo;
      y.innerHTML=factura.fecha;
      z.innerHTML=factura.total;
    }
  }else{
    idsFacturas= [];
  }

}
